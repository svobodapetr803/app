import React from 'react'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'

import { getArticles } from '../actions'
import Articles from '../containers/Articles'


class ArticlesCMP extends React.Component {
    constructor(props) {
        super(props);
        this.q = "";
        this.begin_date = "";
        this.end_date = new Date();
        this.minDate = "";
        this.sort = "";
    }

    nextPage() {
        let i = this.props.match.params.page === undefined ? 0 : this.props.match.params.page;
        if (i < ((this.props.data.response.meta.hits) / 100) + 1) {
            i++;
            this.props.history.push("/articles/" + this.props.match.params.q + "/" + i +
                (this.begin_date.value === "" ? "/1851-09-18" : "/" + this.begin_date.value) + (this.end_date.value === "" ? "" : "/" + this.end_date.value));
        }
    }

    previousPage() {
        let i = this.props.match.params.page;
        if (i > 0) {
            let i = this.props.match.params.page === undefined ? 1 : this.props.match.params.page;
            i--;
            this.props.history.push("/articles/" + this.props.match.params.q + "/" + i +
                (this.begin_date.value === "" ? "/1851-09-18" : "/" + this.begin_date.value) + (this.end_date.value === "" ? "" : "/" + this.end_date.value));
        }
    }

    setMinDate() {
        let x = document.getElementById("bd").value;
        document.getElementById("ed").min = x;
    }

    setMaxDate() {
        let x = document.getElementById("ed").value;
        document.getElementById("bd").max = x;
    }

    render() {
        return (
            <div>
                <div>
                    <h1 className="position-absolute" >Articles search</h1>
                    <Link to="/" className="btn btn-warning float-right mb-3" >Back</Link>
                    <form onSubmit={e => {
                        e.preventDefault();
                        for (let i = 0; i < document.getElementsByName("sortrad").length; i++) {
                            if (document.getElementsByName("sortrad")[i].checked) {
                                this.sort = document.getElementsByName("sortrad")[i].value;
                            }
                        }
                        this.props.history.push("/articles/" + (this.q.value === "" ? "undefined" : this.q.value) + "/0" + (this.begin_date.value === "" ? "/1851-09-18" :
                            "/" + this.begin_date.value) + (this.end_date.value === "" ? "/" + this.end_date.max : "/" + this.end_date.value) + "/" + this.sort);
                    }}>
                        <div className="form-group">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">Keyword</div>
                                </div>
                                <input ref={node => this.q = node} type="text" className="form-control" placeholder="Enter some word" />
                            </div>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">Publication date (from)</div>
                                </div>
                                <input ref={node => this.begin_date = node} type="date" id="bd" className="form-control" min="1851-09-18" onChange={this.setMinDate}
                                    max={(new Date().getFullYear()) + "-" + ((new Date().getMonth() + 1) >= 10 ? new Date().getMonth() + 1 : "0" + (new Date().getMonth() + 1)) + "-"
                                        + (new Date().getDate() >= 10 ? new Date().getDate() : "0" + (new Date().getDate()))} />
                            </div>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">Publication date (to)</div>
                                </div>
                                <input ref={node => this.end_date = node} type="date" id="ed" className="form-control" min="" onChange={this.setMaxDate}
                                    max={(new Date().getFullYear()) + "-" + ((new Date().getMonth() + 1) >= 10 ? new Date().getMonth() + 1 : "0" + (new Date().getMonth() + 1)) + "-"
                                        + (new Date().getDate() >= 10 ? new Date().getDate() : "0" + (new Date().getDate()))} />
                            </div>
                            <div className="d-flex">
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="sortrad" value="newest" />
                                    <label className="form-check-label" >Newest first</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="sortrad" value="oldest" />
                                    <label className="form-check-label">Oldest first</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="sortrad" value="" defaultChecked />
                                    <label className="form-check-label" >No sort</label>
                                </div>
                                <button type="submit" className="btn btn-outline-secondary ml-auto" >Submit</button>
                            </div>
                            <div className="d-flex">
                                <small className="form-text text-muted">Just wait, it can take a while...</small>
                                <small className="form-text text-muted ml-auto">NYT Articles search by Petr Svoboda</small>
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                    <h3 className={"d-flex justify-content-center " + this.props.message} >{this.props.message === "loader" ? null : this.props.message}</h3>
                    {this.props.data.response === undefined ? null :
                        <div>
                            {this.props.data.response.meta.hits < 10 ? null :
                                <div>
                                    <button className="btn btn-danger" onClick={this.previousPage.bind(this)} >Previous page</button>
                                    <button className="btn btn-success float-right" onClick={this.nextPage.bind(this)} >Next page</button>
                                </div>
                            }
                            <div>
                                {this.props.data.response.meta.hits === 0 ? <h3 className="d-flex justify-content-center" >No results...</h3> :
                                    <Articles docs={this.props.data.response.docs} />
                                }
                            </div>
                            {this.props.data.response.meta.hits < 10 ? null :
                                <div>
                                    <button className="btn btn-danger" onClick={this.previousPage.bind(this)} >Previous page</button>
                                    <button className="btn btn-success float-right" onClick={this.nextPage.bind(this)} >Next page</button>
                                </div>
                            }
                        </div>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.data,
    message: state.message
})

const mapDispatchToProps = (dispatch, props) => {
    dispatch(getArticles(props.match.params.q, props.match.params.page,
        props.match.params.begin_date, props.match.params.end_date,
        props.match.params.sort));
    return { getArticles }
}

const ArticlesSearch = connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(ArticlesCMP))

export default ArticlesSearch