import React from 'react'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import ReactTable from "react-table";
import "react-table/react-table.css";

import { getArchive } from '../actions'


class ArchiveCMP extends React.Component {
    constructor() {
        super();
        this.input = "";
    }

    render() {
        return (
            <div>
                <h1 className="position-absolute" >Reviews search</h1>
                <Link to="/" className="btn btn-warning float-right mb-3" >Back</Link>
                <form onSubmit={e => {
                    e.preventDefault();
                    if (!this.input.value.trim()) { return }
                    this.props.history.push("/archive/" + this.input.value.slice(0, 4) + "/" + this.input.value.slice(5, 7).replace(/(^|-)0+/g, "$1"));
                }}>
                    <div className="form-group clsform">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <div className="input-group-text">Date</div>
                            </div>
                            <input type="month" min="1851-01" ref={node => this.input = node} className="form-control" />
                            <div className="input-group-append">
                                <button type="submit" className="btn btn-outline-secondary" >Submit</button>
                            </div>
                        </div>
                        <div className="d-flex">
                            <small className="form-text text-muted">Just wait, it can take a while...</small>
                            <small className="form-text text-muted ml-auto">NYT Archive search by Petr Svoboda</small>
                        </div>
                    </div>
                </form>
                <div>
                    <h3 className={"d-flex justify-content-center " + this.props.message} >{this.props.message === "loader" ? null : this.props.message}</h3>
                    {this.props.data.response === undefined ? null :
                        <ReactTable
                            data={this.props.data.response.docs}
                            columns={[
                                {
                                    Header: "Title",
                                    columns: [
                                        {
                                            Header: "Title",
                                            accessor: "headline.main",
                                        },
                                        {
                                            Header: "Publicattion date",
                                            accessor: "pub_date",
                                        },
                                        {
                                            Header: "Headline",
                                            accessor: "headline.print_headline",
                                        },
                                        {
                                            Header: "Detail",
                                            accessor: "snippet"
                                        }
                                    ],
                                },
                                {
                                    Header: "Author",
                                    columns: [
                                        {
                                            Header: "First name",
                                            accessor: "byline.person[0].firstname",
                                        },
                                        {
                                            Header: "Last name",
                                            accessor: "byline.person[0].lastname"
                                        }
                                    ],
                                },
                                {
                                    Header: "Document info",
                                    columns: [
                                        {
                                            Header: "Type",
                                            accessor: "document_type",
                                        },
                                        {
                                            Header: "Section",
                                            accessor: "section_name",
                                        },
                                        {
                                            Header: "URL",
                                            accessor: "web_url",
                                            Cell: e => <a href={e.value}>Link</a>,
                                        },
                                    ],
                                },
                            ]}
                            defaultSorted={[
                                {
                                    id: "pub_date",
                                    desc: false
                                }
                            ]}
                            defaultPageSize={10}
                            className="-striped -highlight"
                        />
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.data,
    message: state.message
})

const mapDispatchToProps = (dispatch, props) => {
    dispatch(getArchive(props.match.params.year === undefined ? new Date().getFullYear() : props.match.params.year,
        props.match.params.month === undefined ? new Date().getMonth() : props.match.params.month));
    return {}
}

const ArchiveSearch = connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(ArchiveCMP))

export default ArchiveSearch