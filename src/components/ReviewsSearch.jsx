import React from 'react'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'

import { getReviews } from '../actions'
import Reviews from '../containers/Reviews'


class ReviewsCMP extends React.Component {
    constructor() {
        super();
        this.input = "";
    }

    addToFavorite() {
        console.log(this);
        /*let favorite = {};
        let type = fav_btn.getAttribute("data-type");
        favorite["type"] = type;
        fav_btn.parentElement.querySelectorAll("[data-level]").forEach((level) => {
            favorite[level.getAttribute("data-level")] = level.textContent;
        })
        console.log(favorite);*/
    }

    render() {
        return (
            <div>
                <div>
                    <h1 className="position-absolute" >Reviews search</h1>
                    <Link to="/" className="btn btn-warning float-right mb-3" >Back</Link>
                    <form onSubmit={e => {
                        e.preventDefault();
                        if (!this.input.value.trim()) { return }
                        this.props.history.push("/reviews/" + this.input.value);
                    }}>
                        <div className="form-group clsform">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">Keyword</div>
                                </div>
                                <input ref={node => this.input = node} className="form-control" placeholder="Enter some word" />
                                <div className="input-group-append">
                                    <button type="submit" className="btn btn-outline-secondary" >Submit</button>
                                </div>
                            </div>
                            <div className="d-flex">
                                <small className="form-text text-muted">Just wait, it can take a while...</small>
                                <small className="form-text text-muted ml-auto">NYT Reviews search by Petr Svoboda</small>
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                    <h3 className={"d-flex justify-content-center " + this.props.message} >{this.props.message === "loader" ? null : this.props.message}</h3>
                    {this.props.data === undefined ? null :
                        <div>
                            {this.props.data.num_results === 0 ? <h3 className="d-flex justify-content-center" >No results...</h3> :
                                <Reviews results={this.props.data.results} />
                            }
                        </div>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.data,
    message: state.message
})

const mapDispatchToProps = (dispatch, props) => {
    dispatch(getReviews(props.match.params.query));
    return { getReviews }
}

const ReviewsSearch = connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(ReviewsCMP))

export default ReviewsSearch