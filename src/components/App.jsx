import React from 'react'
import { withRouter, Link } from 'react-router-dom'


const App = () => (
  <div className="center" >
    <Link to="/Articles" className="btn btn-primary" >Articles search</Link>
    <Link to="/Archive" className="btn btn-primary" >Search throught archive</Link>
    <Link to="/Reviews" className="btn btn-primary" >Movie reviews search</Link>
    <Link to="/Favorites" className="btn btn-primary" >Favorites</Link>
  </div>
)

export default withRouter(App);
