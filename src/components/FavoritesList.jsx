import React from 'react'
import { withRouter, Link } from 'react-router-dom'


class FavoritesList extends React.Component {
    removeFromFavorite(e) {
        let fav_btn = e.target;
        let favorite = {};
        let type = fav_btn.getAttribute("data-type");
        favorite["type"] = type;

        fav_btn.parentElement.querySelectorAll("[data-level]").forEach((level) => {
            if (level.getAttribute("data-level") === "link") {
                favorite[level.getAttribute("data-level")] = level.getAttribute("href");
            } else {
                favorite[level.getAttribute("data-level")] = level.textContent;
            }
        })

        let favs = [];
        let found = false;

        if (window.localStorage.getItem("favorites") === null) {
            favs.push(favorite);
            window.localStorage.setItem("favorites", JSON.stringify(favs));
            fav_btn.setAttribute("src", "../heart-filled.svg");
        } else {
            favs = JSON.parse(window.localStorage.getItem("favorites"));

            for (let i = 0; i < favs.length; i++) {
                if (favs[i].title === favorite.title && favs[i].type === favorite.type) {
                    found = true;
                    favs.splice(i, 1);
                    window.localStorage.setItem("favorites", JSON.stringify(favs));
                    fav_btn.setAttribute("src", "../heart-empty.svg");
                    break;
                }
            }

            if (!found) {
                favs.push(favorite);
                window.localStorage.setItem("favorites", JSON.stringify(favs));
                fav_btn.setAttribute("src", "../heart-filled.svg");
            }
        }
    }

    createFavorites() {
        let favs = JSON.parse(window.localStorage.getItem("favorites"))
        let items = [];

        for (let i = 0; i < favs.length; i++) {
            let fav = favs[i];
            let item = (
                <div key={i}>
                    <div className="list-group-item list-group-item-action flex-column align-items-start d-flex">
                        <img className="fav_btn" data-type="article" onClick={this.removeFromFavorite.bind(this)} alt="heart" src="../heart-filled.svg" height="28px" />
                        <div className="d-block">
                            <h5 className="mb-2" data-level="title">{fav.title}</h5>
                            <h6 className="mb-1" data-level="subtitle">{fav.subtitle}</h6>
                            {fav.pubDate === undefined ? null : <p className="text-muted mb-1" data-level="pubDate">{fav.pubDate}</p>}
                            {fav.image === undefined ? null : <img className="img-fluid rounded d-block" data-level="image" src={fav.image} alt="Scene from the movie" />}
                            <p className="text-muted mb-3" data-level="text">{fav.text}</p>
                        </div>
                        <a href={fav.link} className="d-block text-muted" data-level="link">Read it now.</a>
                        {fav.author === undefined ? null : <p className="d-block ml-auto" data-level="author">{fav.author}</p>}
                    </div>
                </div>
            )
            items.push(item);
        }
        return items;
    }

    render() {
        return (
            <div>
                <div>
                    <h1 className="position-absolute" >Favorites</h1>
                    <Link to="/" className="btn btn-warning float-right mb-3" >Back</Link>
                </div>
                <div className="pos-rel">
                    {window.localStorage.getItem("favorites") === "[]" || window.localStorage.getItem("favorites") === null ?
                        <h3 className="txt-center" >No favorites, click on heart to add to favorites...</h3> :
                        this.createFavorites()
                    }
                </div>
            </div>
        )
    }

}

export default withRouter(FavoritesList);
