import React from 'react'
import { withRouter } from 'react-router-dom'

import Review from './Review'


class Reviews extends React.Component {
    /*constructor(props){
        super(props);
        this.items = props.items;
        
        if(this.items.length > 10){
            this.items = this.items.slice(0, 10);
        }
        
    }*/

    render() {
        return (
            <div className="list-group">
                { this.props.results === undefined ? null :
                    this.props.results.map((result, i) => {
                        return <Review result={result} key={i} />
                    })
                }
            </div>
        )
    }

    state = { result: this.props.results }
}

export default withRouter(Reviews)