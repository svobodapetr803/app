import React from 'react'
import { withRouter } from 'react-router-dom'


class Review extends React.Component {
    addToFavorite(e) {
        let fav_btn = e.target;
        let favorite = {};
        let type = fav_btn.getAttribute("data-type");
        favorite["type"] = type;

        fav_btn.parentElement.querySelectorAll("[data-level]").forEach((level) => {
            let levelName = level.getAttribute("data-level");
            if (levelName === "link") {
                favorite[levelName] = level.getAttribute("href");
            } else if (levelName === "image") {
                favorite[levelName] = level.getAttribute("src");
            } else {
                favorite[levelName] = level.textContent;
            }
        })

        let favs = [];
        let found = false;

        if (window.localStorage.getItem("favorites") === null) {
            favs.push(favorite);
            window.localStorage.setItem("favorites", JSON.stringify(favs));
            fav_btn.setAttribute("src", "../heart-filled.svg");
        } else {
            favs = JSON.parse(window.localStorage.getItem("favorites"));

            for (let i = 0; i < favs.length; i++) {
                if (favs[i].title === favorite.title && favs[i].type === favorite.type) {
                    found = true;
                    favs.splice(i, 1);
                    window.localStorage.setItem("favorites", JSON.stringify(favs));
                    fav_btn.setAttribute("src", "../heart-empty.svg");
                    break;
                }
            }

            if (!found) {
                favs.push(favorite);
                window.localStorage.setItem("favorites", JSON.stringify(favs));
                fav_btn.setAttribute("src", "../heart-filled.svg");
            }
        }
    }

    setSrc() {
        let favs = JSON.parse(window.localStorage.getItem("favorites"));
        if (favs !== null && favs.length > 0) {
            for (let i = 0; i < favs.length; i++) {
                if (favs[i].title === this.fav_btn.parentElement.querySelector("[data-level='title']").textContent && favs[i].type === this.fav_btn.getAttribute("data-type")) {
                    this.fav_btn.setAttribute("src", "../heart-filled.svg");
                }
            }
        }
    }

    render() {
        return (
            <div>
                <div className="list-group-item list-group-item-action flex-column align-items-start">
                    <img className="fav_btn" data-type="article" onClick={this.addToFavorite.bind(this)} alt="heart" src="../heart-empty.svg" height="28px" ref={c => this.fav_btn = c} />
                    <div className="d-block w-100 justify-content-between">
                        <h4 className="mb-2" data-level="title">{this.props.result.display_title}</h4>
                        <h5 className="mb-1" data-level="subtitle">{this.props.result.headline}</h5>
                        <p className="text-muted mb-1" data-level="pubDate">{this.props.result.date_updated}</p>
                        {this.props.result.multimedia !== null ? <img className="img-fluid rounded d-block" data-level="image" alt="Scene from the movie" src={this.props.result.multimedia.src} /> : null}
                        <p className="text-muted mb-3" data-level="text">{this.props.result.summary_short}</p>
                    </div>
                    <div className="d-flex">
                        <a href={this.props.result.link.url} className="text-muted" data-level="link">Read it now.</a>
                        <p className="text-muted ml-auto" data-level="author">{this.props.result.byline}</p>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.setSrc(this.fav_btn);
    }

    componentDidUpdate() {
        this.setSrc(this.fav_btn);
    }
}

export default withRouter(Review);