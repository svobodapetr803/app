import React from 'react'
import { withRouter } from 'react-router-dom'

import Article from './Article'


class Articles extends React.Component {
    render() {
        return (
            <div className="list-group">
                {
                    this.props.docs.map((doc, i) => {
                        return <Article doc={doc} key={i} />
                    })
                }
            </div>
        )
    }

    state = { doc: this.props.docs }
}

export default withRouter(Articles)