import React from 'react'
import { withRouter } from 'react-router-dom'


class Article extends React.Component {
    formatPubDate(time) {
        let date = new Date((time || ""));
        let hour = ("0" + date.getHours()).slice(-2);
        let min = ("0" + date.getMinutes()).slice(-2);
        let sec = ("0" + date.getSeconds()).slice(-2);

        let day = ("0" + date.getDate()).slice(-2);
        let month = ("0" + (date.getMonth() + 1)).slice(-2);
        let year = date.getFullYear();

        return hour + ":" + min + ":" + sec + " " + day + "." + month + "." + year;
    }

    addToFavorite(e) {
        let fav_btn = e.target;
        let favorite = {};
        let type = fav_btn.getAttribute("data-type");
        favorite["type"] = type;

        fav_btn.parentElement.querySelectorAll("[data-level]").forEach((level) => {
            if (level.getAttribute("data-level") === "link") {
                favorite[level.getAttribute("data-level")] = level.getAttribute("href");
            } else {
                favorite[level.getAttribute("data-level")] = level.textContent;
            }
        })

        let favs = [];
        let found = false;

        if (window.localStorage.getItem("favorites") === null) {
            favs.push(favorite);
            window.localStorage.setItem("favorites", JSON.stringify(favs));
            fav_btn.setAttribute("src", "../heart-filled.svg");
        } else {
            favs = JSON.parse(window.localStorage.getItem("favorites"));

            for (let i = 0; i < favs.length; i++) {
                if (favs[i].title === favorite.title && favs[i].type === favorite.type) {
                    found = true;
                    favs.splice(i, 1);
                    window.localStorage.setItem("favorites", JSON.stringify(favs));
                    fav_btn.setAttribute("src", "../heart-empty.svg");
                    break;
                }
            }

            if (!found) {
                favs.push(favorite);
                window.localStorage.setItem("favorites", JSON.stringify(favs));
                fav_btn.setAttribute("src", "../heart-filled.svg");
            }
        }
    }

    setSrc() {
        let favs = JSON.parse(window.localStorage.getItem("favorites"));
        if (favs !== null && favs.length > 0) {
            for (let i = 0; i < favs.length; i++) {
                if (favs[i].title === this.fav_btn.parentElement.querySelector("[data-level='title']").textContent && favs[i].type === this.fav_btn.getAttribute("data-type")) {
                    this.fav_btn.setAttribute("src", "../heart-filled.svg");
                }
            }
        }
    }

    render() {
        return (
            <div>
                <div className="list-group-item list-group-item-action flex-column align-items-start">
                    <img className="fav_btn" data-type="article" onClick={this.addToFavorite.bind(this)} alt="heart" src="../heart-empty.svg" height="28px" ref={c => this.fav_btn = c} />
                    <div className="d-block w-100 justify-content-between">
                        <h4 className="mb-2" data-level="title">{this.props.doc.headline.main}</h4>
                        <h5 className="mb-1" data-level="subtitle">{this.props.doc.headline.print_headline}</h5>
                        {this.props.doc.pub_date === undefined ? null : <p className="text-muted mb-1" data-level="pubDate">{this.formatPubDate(this.props.doc.pub_date)}</p>}
                        <small className="text-muted mb-3" data-level="text">{this.props.doc.snippet}</small>
                    </div>
                    <div className="d-flex">
                        <a href={this.props.doc.web_url} className="text-muted" data-level="link">Read it now.</a>
                        {this.props.doc.byline === undefined ? null : <p className="text-muted ml-auto" data-level="author">{this.props.doc.byline.original}</p>}
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.setSrc(this.fav_btn);
    }

    componentDidUpdate() {
        this.setSrc(this.fav_btn);
    }
}

export default withRouter(Article);