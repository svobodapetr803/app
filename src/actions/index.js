import axios from "axios";

const API_URL = "https://api.nytimes.com/svc/";
const api_key = "y1dCvP7PZdetsbPfI08WZtN2gJlSGvnL"


export const ARCHIVE_ACTION = 'ARCHIVE_ACTION';
export const ARTICLES_ACTION = 'ARTICLES_ACTION';
export const REVIEWS_ACTION = 'REVIEWS_ACTION';


export const getArchive = (year, month) => {
    const request = axios.get(API_URL + "archive/v1/" + year + "/" + (month === 0 ? 1 : month) + ".json", {
        params: {
            "api-key": api_key,
        }
    });
    return {
        type: ARCHIVE_ACTION,
        payload: request
    }
}

export const getArticles = (q, page, begin_date, end_date, sort) => {
    const request = axios.get(API_URL + "search/v2/articlesearch.json", {
        params: {
            "api-key": api_key,
            q: q === "undefined" ? undefined : q,
            page: page,
            begin_date: begin_date,
            end_date: end_date,
            sort: sort,
        }
    });
    return {
        type: ARTICLES_ACTION,
        payload: request
    }
}

export const getReviews = (query) => {
    const request = axios.get(API_URL + "movies/v2/reviews/search.json", {
        params: {
            query: query,
            "api-key": api_key
        }
    });
    return {
        type: REVIEWS_ACTION,
        payload: request
    }
}