import { ARCHIVE_ACTION, ARTICLES_ACTION, REVIEWS_ACTION } from "../actions";

const processReducer = (state = { data: "" }, action) => {
  switch (action.type) {
    case ARCHIVE_ACTION + "_PENDING":
    case ARTICLES_ACTION + "_PENDING":
    case REVIEWS_ACTION + "_PENDING":
      {
        return { ...state, message: "loader" };
      }
    case ARCHIVE_ACTION + "_FULFILLED":
      {
        return { data: action.payload.data };
      }
    case ARTICLES_ACTION + "_FULFILLED":
      {
        return { data: action.payload.data };
      }
    case REVIEWS_ACTION + "_FULFILLED":
      {
        return { data: action.payload.data };
      }
    case ARCHIVE_ACTION + "_REJECTED":
    case ARTICLES_ACTION + "_REJECTED":
    case REVIEWS_ACTION + "_REJECTED":
      {
        console.log("REJECTED");
        return { ...state, message: "No internet/Error see console" };
      }
    default:
      return state
  }
}

export default processReducer
