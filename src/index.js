import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise-middleware';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from "react-router-dom";

import App from './components/App';
import ArchiveSearch from './components/ArchiveSearch';
import ArticlesSearch from './components/ArticlesSearch';
import ReviewsSearch from './components/ReviewsSearch';
import FavoritesList from './components/FavoritesList';
import reducer from './reducers';

import "./style.css"
import 'bootstrap/dist/css/bootstrap.css'

const storeWM = createStore(reducer, applyMiddleware(promise));

render(
    <Provider store={storeWM}>
        <BrowserRouter>
            <Switch>
                <Route path="/archive/:year?/:month?" component={ArchiveSearch} />
                <Route path="/articles/:q?/:page?/:begin_date?/:end_date?/:sort?" component={ArticlesSearch} />
                <Route path="/reviews/:query?" component={ReviewsSearch} />
                <Route path="/favorites" component={FavoritesList} />
                <Route path="/" component={App} />
            </Switch >
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
)